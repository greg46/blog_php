var gulp = require('gulp');
var sass = require('gulp-sass');
var cleanCss = require('gulp-clean-css');
var minifyJS = require('gulp-uglify');
var browserSync = require('browser-sync');
var php  = require('gulp-connect-php');

gulp.task('sass', function () {
  gulp.src('./assets/scss/*.scss')
    .pipe(sass())
    .pipe(cleanCss())
    .pipe(gulp.dest('./dist/css/'))
});

gulp.task('minifyJS', function () {
  gulp.src('./assets/js/*.js')
    .pipe(minifyJS())
    .pipe(gulp.dest('./dist/js/'))
});

gulp.task('serve', function() {

  php.server({
      port: 80,             // Port (8000 par défaut)
      base: './dist' // Base du projet
  });
  });


gulp.task('browser-sync', function () {
  browserSync.init({
    server: "./dist/"
  });
});

gulp.task('gulpwatch', ['serve', 'sass', 'minifyJS', 'browser-sync'], function () {
  gulp.watch('./assets/scss/**.scss', ['sass']).on('change', browserSync.reload);
  gulp.watch('./assets/js/*.js', ['minifyJS']).on('change', browserSync.reload);
  gulp.watch('./dist/*.php').on('change', browserSync.reload);
});

gulp.task('default', ['gulpwatch']);
